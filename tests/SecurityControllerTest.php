<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SecurityControllerTest.
 */
class SecurityControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/register');
        $button = $crawler->selectButton('Submit');
        $form = $button->form([
            'user[login]' => 'admin',
            'user[password][first]' => 'admin',
            'user[password][second]' => 'admin',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('Sign in');
        $form = $button->form([
            'username' => 'admin',
            'password' => 'admin',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
